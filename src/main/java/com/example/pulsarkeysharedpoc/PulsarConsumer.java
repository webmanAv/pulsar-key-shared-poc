package com.example.pulsarkeysharedpoc;

import org.apache.pulsar.client.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.*;

@Component
public class PulsarConsumer {
    private final PulsarClient pulsarClient;
    private final ExecutorService executorService;
    private List<Future> futureList;

    @Autowired
    public PulsarConsumer(PulsarClient pulsarClient){
        this.pulsarClient = pulsarClient;
        this.executorService = Executors.newFixedThreadPool(5);
        futureList = new ArrayList<>();
    }

    public void killConsumer(int i){
        futureList.get(i).cancel(true);
    }


    public void manager(){
        try {
            for (int i=0; i<5; i++) {
                Thread.sleep(10000);
                createConsumer(i);
            }

            for (int i=0; i<5; i++) {
                Thread.sleep(10000);
                killConsumer(i);
            }

            for (int i=0; i<5; i++) {
                Thread.sleep(10000);
                createConsumer(i);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @PostConstruct
    public void init(){
        executorService.submit((()->manager()));
    }

    public void createConsumer(int i) {
        int finalI = i;
        futureList.add(i, executorService.submit(() -> {
            consume("consumer" + finalI);
        }));
    }

    public void consume(String name){
        Consumer<byte[]> consumer = null;
        try {
            consumer = createPulsarConsumer();
            SortedSet<Integer> set = new ConcurrentSkipListSet<>();
            long lastCleanupTime = System.currentTimeMillis();
            while (!Thread.interrupted()) {
                Message<byte[]> msg = consumer.receive();
                set.add(Integer.parseInt(msg.getKey()));
                //System.out.println(name + "," + msg.getKey() + "," + new String(msg.getData()));
                consumer.acknowledge(msg);
                if (System.currentTimeMillis() - lastCleanupTime > 2000) {
                    System.out.println(name + " set:" + set);
                    lastCleanupTime = System.currentTimeMillis();
                    set.clear();
                }
            }
        }catch (Exception ex){
            if (consumer != null){
                try {
                    consumer.close();
                } catch (PulsarClientException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public org.apache.pulsar.client.api.Consumer<byte[]> createPulsarConsumer() throws PulsarClientException {
        return pulsarClient.newConsumer()
                .topic(System.getenv("TARGET_TOPIC"))
                .subscriptionType(SubscriptionType.Key_Shared)
                .subscriptionName("bla")
                .subscribe();
    }
}
