package com.example.pulsarkeysharedpoc;

import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PulsarConfig {
    @Bean
    public PulsarClient createPulsarClient() throws PulsarClientException {
        String pulsarHost = System.getenv("PULSAR_HOST");
        String pulsarPort = System.getenv("PULSAR_PORT");

        return PulsarClient.builder()
                .serviceUrl("pulsar://"+ pulsarHost + ":" +pulsarPort)
                .build();
    }
}
