package com.example.pulsarkeysharedpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulsarKeySharedPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(PulsarKeySharedPocApplication.class, args);
	}

}
