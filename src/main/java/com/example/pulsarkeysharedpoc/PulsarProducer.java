package com.example.pulsarkeysharedpoc;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
@Component
public class PulsarProducer {
    private final PulsarClient pulsarClient;
    private ExecutorService executorService;

    @Autowired
    public PulsarProducer(PulsarClient pulsarClient){
        this.pulsarClient = pulsarClient;
        executorService = Executors.newFixedThreadPool(1);
    }

    @PostConstruct
    public void init(){
        executorService.submit(()->{
            try {
                produce();
            } catch (PulsarClientException e) {
                e.printStackTrace();
            }
        });
    }

    private void produce() throws PulsarClientException {
        Random rand = new Random();
        Producer<byte[]> producer = createPulsarProducer();
        int counter = 0;
        while(true){
            String key =  rand.nextInt(30) +"";
            String msg = "msg" + counter;
            producer.newMessage()
                    .key(key)
                    .value(msg.getBytes())
                    .send();
            counter++;
        }
    }

    private Producer<byte[]> createPulsarProducer() throws PulsarClientException {
        return pulsarClient.newProducer().enableBatching(false)
                .topic(System.getenv("TARGET_TOPIC")).create();
    }
}
